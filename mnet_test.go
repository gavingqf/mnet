package mnet

/*
 * mnet test: rpc call example.
 */

import (
	"fmt"
	"testing"
	"time"
)

// global message dealer
var md *messageDealer = newMessageDealer()

// server session factory.
type sessionFactory struct {
}

func (sf *sessionFactory) CreateSession() ISession {
	rs := CreateSession()
	rs.SetDealer(md.Handle)
	rs.SetEstablish(md.establish)
	return rs
}

// message dealer
type messageDealer struct {
	RPCMessageDealer
}

func newMessageDealer() *messageDealer {
	md := &messageDealer{}
	md.BuildStructMethods(md)
	return md
}

func (md *messageDealer) Handle(s *Session, msgId interface{}, para ...interface{}) {
	switch v := msgId.(type) {
	case uint32:
		return
	case string:
		md.Process(s, v, para...)
	}
}

// rpc call function.
func (md *messageDealer) ProcessHello(sess *Session, name string, id uint32,
	surname string, uuid uint64, fId float64, arr []interface{},
) {
	sess.Call("ProcessWorld", "hello", uint32(121), "world", uint64(1), 123.456, arr)
}

func (md *messageDealer) Hello(sess *Session, name string, id uint32) {
	fmt.Println(name, id)
}

func (md *messageDealer) establish(sess *Session, isConnected bool) {
	if isConnected {
		log().Info(sess.GetConn().RemoteAddr(), " sess is connected")
		sess.SendMsg(1, []byte("hello"))
	} else {
		log().Info(sess.GetConn().RemoteAddr(), " sess is terminate")
	}
}

// /////////////////////////////////////////////////////////////////////////////////////////////////
// rpc调用请求
func TestNet(t *testing.T) {
	// server listen.
	server := GetNetInstance().CreateServer("0.0.0.0:5566", &sessionFactory{}, CreateBigCodec(), nil, false, 1024)
	server.StartListen()
	select {}
}

// 同步消息请求
func TestSyncNet(t *testing.T) {
	serverAddr := "127.0.0.1:5566"
	sess := CreateSyncSession()
	codec := CreateBigCodec()
	client := GetNetInstance().CreateClient(serverAddr, sess, codec, nil)
	client.SyncConnect()

	// start a goroutine.

	// send and resp
	/*
		msgId := 1
		msg := "hello,sync session"
		res, err := sess.Require(uint16(msgId), []byte(msg))
		if err != nil {
			// log().Error("req ", msg, " error:", err)
			return
		}
	*/

	// log out.
	// log().Info("sync send msg id:", msgId, " data:", msg, ", resp msgId:", res.MsgId, ", resp content:", string(res.Resp))

	// async send message
	go func() {
		for {
			sess.Send(1, []byte("hello,sync session"))

			// sleep
			time.Sleep(10 * time.Millisecond)
		}
	}()

	select {}
}
