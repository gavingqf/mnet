package mnet

import "runtime/debug"

/*
 * util module.
 */

func Go(f func()) {
	defer func() {
		if err := recover(); err != nil {
			buf := debug.Stack()
			log().Error("panic: %v\n%s", err, buf)
		}
	}()
	f()
}
