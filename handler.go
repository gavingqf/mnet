package mnet

/*
 * rpc handler
 */
import "reflect"

type RPCMessageDealer struct {
	Dealer map[string]reflect.Value
}

// BuildStructMethods struct object function and return
func (md *RPCMessageDealer) BuildStructMethods(structObj interface{}) {
	md.Dealer = make(map[string]reflect.Value)
	vf := reflect.ValueOf(structObj)
	vft := vf.Type()
	num := vf.NumMethod()
	for i := 0; i < num; i++ {
		mName := vft.Method(i).Name
		md.Dealer[mName] = vf.Method(i)
	}
}

// message rpc function.
func (md *RPCMessageDealer) Process(sess *Session, methodFunc string, data ...interface{}) {
	if dealer, ok := md.Dealer[methodFunc]; ok {
		var paras []reflect.Value = make([]reflect.Value, 0, len(data)+1)
		paras = append(paras, reflect.ValueOf(sess))
		for i := 0; i < len(data); i++ {
			paras = append(paras, reflect.ValueOf(data[i]))
		}
		dealer.Call(paras)
	} else {
		log().Error("can not find ", methodFunc, " dealer")
	}
}
