# mnet：mutiple goroutine net  
The mnet is a multiple goroutine tcp net library. And all interfaces are listed in inet.go

# how to use
 User must implement the following interface:
- 1. ISession
  ISession is user's tcp connection session interface, which controls a tcp connection(establish, terminate, recv data and so.).
  User should keep mnet's IConnection to send message as method: SetConnection(conn IConnection), and must not set the conn to nil.

- 2. ISessionFactory
  session factory is the session factory interface where you can create new session.
- 3. ICodec
  The tcp packet's encode/decode interface

# example
 Please see mnet_test.go for details.
``` go
import (
	"reflect"
	"testing"
	"time"
)

// global message dealer
var md *messageDealer = newMessageDealer()

// buildStructMethods struct object function and return
func buildStructMethods(strcutObj interface{}) map[string]reflect.Value {
	crMap := make(map[string]reflect.Value)

	vf := reflect.ValueOf(strcutObj)
	vft := vf.Type()
	mNum := vf.NumMethod()
	for i := 0; i < mNum; i++ {
		mName := vft.Method(i).Name
		crMap[mName] = vf.Method(i)
	}
	return crMap
}

// server session factory.
type sessionFactory struct{}

func (sf *sessionFactory) CreateSession() ISession {
	rs := CreateSession()
	rs.SetDealer(md.process)
	rs.SetEstablish(md.establish)
	return rs
}

// message dealer
type messageDealer struct {
	dealer map[string]reflect.Value
}

func newMessageDealer() *messageDealer {
	md := &messageDealer{}
	md.init()
	return md
}

func (md *messageDealer) init() {
	md.dealer = buildStructMethods(md)
}

// rpc call function.
func (md *messageDealer) ProcessHello(sess *Session, name string, id int, surname string, uuid uint64, array []int, fId float64) {
	log().Info(name, id, surname, uuid, array, fId)
}

// message processor: uint32 message id or name function.
func (md *messageDealer) process(sess *Session, msgId interface{}, data ...interface{}) {
	switch v := msgId.(type) {
	case string: // name
		if dealer, ok := md.dealer[v]; ok {
			// build function's parameter.
			var paras []reflect.Value
			paras = append(paras, reflect.ValueOf(sess))
			for i := 0; i < len(data); i++ {
				paras = append(paras, reflect.ValueOf(data[i]))
			}
			// call function.
			dealer.Call(paras)
		} else {
			log().Error("can not find ", msgId, " dealer")
		}

	case uint32: // message id.
		log().Info("deal msg id:", msgId, " message: ", string(data[0].([]byte)))

	default:
		panic("invalid msg id type")
	}
}

func (md *messageDealer) establish(sess *Session, isConnected bool) {
	if isConnected {
		log().Info(sess.GetConn().RemoteAddr(), " sess is connected")

		// rpc call
		sess.Call("ProcessHello", "hello", 121, "world", uint64(1), []int{1, 2}, 123.456)

		// message id cal
		sess.Send(1, []byte("hello"))
	} else {
		log().Info(sess.GetConn().RemoteAddr(), " sess is terminate")
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// rpc session.
func TestNet(t *testing.T) {
	// server listen.
	server := GetNetInstance().CreateServer("127.0.0.1:3355", &sessionFactory{}, CreateBigCodec(), nil, false, 1024)
	server.StartListen()

	// create client.
	rs := &Session{
		DealerFunc:    md.process,
		EstablishFunc: md.establish,
	}
	client := GetNetInstance().CreateClient("127.0.0.1:3355", rs, CreateBigCodec(), nil)
	client.StartConnect()

	select {}
}

// test sync session:show how to use SyncSession.
func TestSyncNet() {
	serverAddr := "127.0.0.1:28888"
	sess := CreateSyncSession()
	codec := CreateBigCodec()
	client := GetNetInstance().CreateClient(serverAddr, sess, codec, nil)
	client.SyncConnect()

	// start a goroutine.

	// send and resp
	msgId := 1
	msg := "hello,sync session"
	res, err := sess.Require(uint32(msgId), []byte(msg))
	if err != nil {
		log().Error("req ", msg, " error:", err)
		return
	}

	// log out.
	log().Info("sync send msg id:", msgId, " data:", msg, ", resp msgId:", res.MsgId, ", resp content:", string(res.Resp))

	// async send message
	go func() {
		sess.Send(1, []byte("hello,sync session"))

		// sleep
		time.Sleep(100 * time.Millisecond)
	}()

	select {}
}
```
